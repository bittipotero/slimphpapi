//GRUNT Tips: http://24ways.org/2013/grunt-is-not-weird-and-hard/
//About grunt-sass module: https://github.com/sindresorhus/grunt-sass

module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        autoprefixer: {
            dist: {
                files: {
                    'client/build/main.min.css': 'client/build/main.min.css'
                }
            }
        },


        sass: {
            options: {
                //takes values such as 'nested' and 'compressed' Try 'em out!
                outputStyle: 'compressed',
            },
            dist: {

                files: {
                    'client/build/main.min.css': 'client/styles/main.scss',
                }
            }
        },

        connect: {
            server: {
                options: {
                    keepalive: true,
                    livereload: true
                }
            }
        },

        wiredep: {

            task: {

                // Point to the files that should be updated when
                // you run `grunt wiredep`
                src: [
                  'client/styles/main.scss',
                  //'Gruntfile.js'
                ],
                options: {
                    // See wiredep's configuration documentation for the options
                    // you may pass:
                }
            }
        },
        open: {
            all: {
                // Gets the port from the connect configuration
                path: 'http://localhost:1337'
            }
        },
        concurrent: {
            serve: ['watch', 'connect'],
            sassconc: ['sass', 'jshint'],
            prefixugly: ['autoprefixer','bowerRequirejs'],
            options: {
                logConcurrentOutput: true
            }
        },

        jshint: {
            all: ["Gruntfile.js","client/js/**/*.js"],
            options: {
                curly: true,
                eqeqeq: true,
                eqnull: true,
                browser: true,
                globals: {
                    jQuery: true
                },
            },
 

        },

        bowerRequirejs: {
            target: {
                rjsConfig: 'client/js/main.js'
            }
        },

        watch: {
            options: {
            },

            css: {
                files: ['client/styles/main.scss'],
                tasks: ['wiredep','sass','autoprefixer'],
            },

            requirejs: {
                files:['bower_components/**/*'],
                tasks: ['bowerRequirejs','wiredep','sass'],
            },


        }


    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('default', ['wiredep', 'concurrent:sassconc', 'concurrent:prefixugly', 'watch']);
    grunt.registerTask('build', ['wiredep', 'concurrent:sassconc', 'concurrent:prefixugly']);
    grunt.registerTask('serve', ['build','open', 'concurrent:serve']);
};
