define([
  'underscore',
  'backbone',
  'models/employee'
], function (_, Backbone, EmployeeModel) {
    var EmployeesCollection = Backbone.Collection.extend({
        url: "api.php/employees",
        model: EmployeeModel
    });
    // You don't usually return a collection instantiated
    return EmployeesCollection;
});