﻿define([
  'underscore',
  'backbone',
  'models/user'
], function (_, Backbone, UserModel) {
    var UsersCollection = Backbone.Collection.extend({
        model: UserModel
    });
    // You don't usually return a collection instantiated
    return UsersCollection;
});