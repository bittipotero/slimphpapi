// Filename: main.js

// Require.js allows us to configure shortcut alias
// There usage will become more apparent further along in the tutorial.
require.config({
    paths: {
        backbone: "../../bower_components/backbone/backbone",
        handlebars: "../../bower_components/handlebars/handlebars",
        requirejs: "../../bower_components/requirejs/require",
        text: "../../bower_components/text/text",
        underscore: "../../bower_components/underscore/underscore",
        jquery: "../../bower_components/jquery/dist/jquery",
        "bootstrap-sass": "../../bower_components/bootstrap-sass/assets/javascripts/bootstrap"
    },
    packages: [

    ]
});

require([

  // Load our app module and pass it to our definition function
  'app',
], function (App) {
    // The "app" dependency is passed in as "App"
    App.initialize();
});