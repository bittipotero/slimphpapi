define([
  'underscore',
  'backbone'
  ], function (_, Backbone) {
    var EmployeeModel = Backbone.Model.extend({
    	url: "api.php/employees/:id",
        defaults: {
            firstName: "",
            lastName:"",
            title:"",
            officePhone: "",
            cellPhone: "",
            email: ""
        }
  });
    // Return the model for the module
    return EmployeeModel;
});