﻿// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'views/users/list',
  'views/root/root',
  'views/employees/list',
  'views/navbar/navbar',
  'collections/employees'

  ], function ($, _, Backbone, usersListView, rootView, employeesView, navbarView, employeeCollection) {




  var AppRouter = Backbone.Router.extend({
    routes: {
      '':'root',
      'users': 'users',
      'employees':'employees',
      '*actions': 'defaultAction'
    }
  });

  var initialize = function () {

    var navbarV = new navbarView();
    navbarV.render();

    var app_router = new AppRouter();

    app_router.on('route:users', function () {
      var usersV = new usersListView();
      usersV.render();
    });

    app_router.on('route:root', function (options) {
       var fragment = Backbone.history.getFragment();

      var rootV = new rootView({fragment:fragment});
      rootV.render();
    });


    app_router.on('route:employees', function () {
        var fragment = Backbone.history.getFragment();

        console.log(fragment);
            //This stuff is bad - put logic to a controller or somewhere else..
            var c =  new employeeCollection();
            var x =  c.fetch({
              error: function (errorResponse) {
                console.log("ERROR");
                console.log(errorResponse);
              },

              success: function(res, collection){
                var data = collection;
                var employeesV = new employeesView({data: data, fragment: fragment});

                employeesV.render();
              }
            });

          });


    app_router.on('defaultAction', function (actions) {
            // We have no matching route, lets just log what the URL was
            console.log('No route:', actions);
          });



        //Backbone.history.start({ pushState: true });
        Backbone.history.start();
        //http://stackoverflow.com/questions/8280322/cant-get-backbone-routes-without-hashes/8280389#8280389
        //http://stackoverflow.com/questions/7310230/backbone-routes-without-hashes
      };

      return {
        initialize: initialize
      };
    });