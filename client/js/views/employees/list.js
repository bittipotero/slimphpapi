define([
  'jquery',
  'underscore',
  'backbone',
  'collections/employees',
  'models/employee',
  'text!templates/employee/list.html',
  'handlebars'
  ], function ($, _, Backbone, employeeCollection, employeeModel, employeeListTemplate,handlebars) {

    var employeeListView = Backbone.View.extend({
      el: $("#container"),
      template: handlebars.compile(employeeListTemplate),
      events:{
        'click .list-group-item':'select',
        'click .media img':'openDetails'
      },

      openDetails: function(ev){
        ev.preventDefault();

        var it = $(ev.target);
        var s = it.closest(".media").find(".employee-details");
       it.closest(".media").toggleClass("media-selected");
        s.slideToggle();
      },



      initialize: function (options) {
    



         $("ul.navbar-nav").children("li").removeClass("active");
        $("nav a[href*='#/"+options.fragment+"']").parent("li").addClass("active");
        //hack to refresh events for the element
        this.$el.unbind();
        this.delegateEvents();
        

        this.$el.html(
          this.template({employees: options.data})
          );
       


      }, 
      render: function(){
        console.log("rendering..");

      }
    });

    // Returning instantiated views can be quite useful for having "state"

    return employeeListView;
  });