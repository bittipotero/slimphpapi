define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/navbar/navbar.html',
  'handlebars'
  ], function ($, _, Backbone, navbarTemplate,handlebars) {

    var navbarView = Backbone.View.extend({
      el: $("nav"),
      template: handlebars.compile(navbarTemplate),
      events:{
        'mouseover a':'hoverLink',
       // 'click a':'addSelected'
      },

      hoverLink: function(ev){
      
      },

      addSelected: function(ev){
       $("ul.navbar-nav").children("li").removeClass("active");
       if(!$(ev.target).hasClass("navbar-brand")){
        $(ev.target).parent().addClass("active");
       }
        
      },

      initialize: function () {
        //hack to refresh events for the element
        this.$el.unbind();
        this.delegateEvents();
        

        this.$el.html(
          this.template
          );
       


      }, 
      render: function(){
        console.log("rendering..");

      }
    });

    // Returning instantiated views can be quite useful for having "state"

    return navbarView;
  });