define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/root.html',
  'handlebars',
  'router'
  ], function ($, _,Backbone,rootTemplate,handlebars, router) {

    var rootView = Backbone.View.extend({
      el: $("#container"),
      template: handlebars.compile(rootTemplate),

      initialize: function (options) {
        //this dirty bit is for hilighting a certain link in navbar after navigation
        $("ul.navbar-nav").children("li").removeClass("active");
        $("nav a[href='#/"+options.fragment+"']").parent("li").addClass("active");
        
        this.delegateEvents();
        this.$el.html(this.template);
      },

      render: function(){
        this.delegateEvents();
      }

    });

    // Returning instantiated views can be quite useful for having "state"
    return rootView;
  });