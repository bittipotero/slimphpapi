﻿define([
  'jquery',
  'underscore',
  'backbone',
  'collections/users',
  'models/user',
  'text!templates/user/list.html',
  'handlebars'
], function ($, _, Backbone, UsersCollection, userModel, userListTemplate,handlebars) {
    var userListView = Backbone.View.extend({
        el: $("#master-container"),
        template: handlebars.compile(userListTemplate),
        events:{
          'click':'alert'
        },

        alert: function(){
          console.log("you clicked: " + event.target);
        },
        initialize: function () {

            this.collection = new UsersCollection();
            var newUser = new userModel({ name: "Matti" });
            var newUser2 = new userModel({ name: "Teppo" });
            this.collection.add(newUser);
            this.collection.add(newUser2);
            //var compiledTemplate = _.template(userListTemplate, { users: this.collection.models });
            //this.$el.html(compiledTemplate);
            this.$el.html(
                this.template({users: this.collection.toJSON()})
                );


        }
    });
    // Returning instantiated views can be quite useful for having "state"
    return userListView;
});